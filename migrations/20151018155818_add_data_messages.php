<?php

use Phinx\Migration\AbstractMigration;

class AddDataMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
		$this->query('
			INSERT INTO `messages` (`id`, `name`, `email`, `message`) VALUES
(40, \'dsfd2\', \'sdvf@sfv.com\', \'xcvxcv()$WJFV)($#*JV\'),
(42, \'fsdvfe\', \'dscd@dcsdc.com\', \'fbvdfbdfbLOH\'),
(44, \'dd\', \'sdfds@dfsd.com\', \'dsvsdv\'),
(45, \'fdvfdvfdv\', \'rhkoi@fvfb.com\', \'fdbdfb\'),
(46, \'anmdae\', \'td@vsd.com\', \'csdcname\'),
(48, \'bfd\', \'td@vsd.com\', \'dfbdfb\'),
(49, \'fdbfdb\', \'dscd@dcsdc.com\', \'bvxb f\'),
(50, \'bfdbfd\', \'sdfds@dfsd.com\', \'f bdfbfd dvfsv\');
		');
    }
}
