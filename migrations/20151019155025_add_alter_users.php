	<?php

use Phinx\Migration\AbstractMigration;

class AddAlterUsers extends AbstractMigration
{
    public function up()
    {
		$this->query('
		ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);
		');
    }
	
	public function down()
	{
		$this->query('
			ALTER TABLE  `users` DROP FOREIGN KEY  `users_ibfk_1`;
		');
	}
}
