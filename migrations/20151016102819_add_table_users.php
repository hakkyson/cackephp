<?php

use Phinx\Migration\AbstractMigration;

class AddTableUsers extends AbstractMigration
{
    public function up()
    {
        $this->query('
        CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `login` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
);
        ');
    }

    public function down()
    {
        $this->query('DROP TABLE `users`');
    }
}
