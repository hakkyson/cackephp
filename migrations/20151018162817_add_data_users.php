<?php

use Phinx\Migration\AbstractMigration;

class AddDataUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
		$this->query('
			INSERT INTO `users` (`id`, `group_id`, `login`, `password`) VALUES
(13, 1, \'admin\', \'$2a$10$X8adwK3Fp17FKicGZeANSuvhGE0x8tdBKVc2UFGnUk.mWc7Ltzql2\'),
(14, 2, \'user\', \'$2a$10$7KcaBnli3rs16ROgPvCvZO7HUZKOlZDnp0HM.vSTiriISdeQce126\');

		');
    }
}
