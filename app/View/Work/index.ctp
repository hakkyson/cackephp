<div class="row text-center form-group">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <h1>Сайт-визитка <span class="label label-primary">personalhomepage</span></h1>
    </div>
    <div class="col-xs-2"></div>
</div>
<div class="row text-center form-group">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
       <?  echo $this->Html->image('screenshot.png', array('style' => "width: 80%; height: 40%;")); ?>
    </div>
    <div class="col-xs-2"></div>
</div>
<div class="row text-center form-group">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <p>На стажировке в a2design, мы делаем сайтик, на примере которого разбираемся в различных тонкостях веб-разработки</p>
        <span class="label label-danger"><a href="https://bitbucket.org/hakkyson/personalhomepage">ТЫЦ</a></span>
    </div>
    <div class="col-xs-2"></div>
</div>